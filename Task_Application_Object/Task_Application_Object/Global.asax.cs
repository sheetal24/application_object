﻿using System;

namespace Task_Application_Object
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Application["ApplicationUser"] = 0;


        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application["ApplicationUser"] = (int)Application["ApplicationUser"] + 1;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application["ApplicationUser"] = (int)Application["ApplicationUser"] - 1;
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}